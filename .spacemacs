;; -*- mode: emacs-lisp -*-
;; This file is loaded by Spacemacs at startup.
;; It must be stored in your home directory.

(defun dotspacemacs/layers ()
  "Configuration Layers declaration.
You should not put any user code in this function besides modifying the variable
values."
  (setq-default
   ;; Base distribution to use. This is a layer contained in the directory
   ;; `+distribution'. For now available distributions are `spacemacs-base'
   ;; or `spacemacs'. (default 'spacemacs)
   dotspacemacs-distribution 'spacemacs

   ;; Lazy installation of layers (i.e. layers are installed only when a file
   ;; with a supported type is opened). Possible values are `all', `unused'
   ;; and `nil'. `unused' will lazy install only unused layers (i.e. layers
   ;; not listed in variable `dotspacemacs-configuration-layers'), `all' will
   ;; lazy install any layer that support lazy installation even the layers
   ;; listed in `dotspacemacs-configuration-layers'. `nil' disable the lazy
   ;; installation feature and you have to explicitly list a layer in the
   ;; variable `dotspacemacs-configuration-layers' to install it.
   ;; (default 'unused)
   dotspacemacs-enable-lazy-installation 'unused

   ;; If non-nil then Spacemacs will ask for confirmation before installing
   ;; a layer lazily. (default t)
   dotspacemacs-ask-for-lazy-installation t

   ;; If non-nil layers with lazy install support are lazy installed.
   ;; List of additional paths where to look for configuration layers.
   ;; Paths must have a trailing slash (i.e. `~/.mycontribs/')
   dotspacemacs-configuration-layer-path '()

   ;; List of configuration layers to load.
   dotspacemacs-configuration-layers
   '(shell-scripts
     typescript
     ;; ----------------------------------------------------------------
     ;; Example of useful layers you may want to use right away.
     ;; Uncomment some layer names and press <SPC f e R> (Vim style) or
     ;; <M-m f e R> (Emacs style) to install them.
     ;; ----------------------------------------------------------------
     ;; LAYERS FOR MAY BE IMORTED
     ;; markdown
     ;; shell-scripts
     ;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; KAPATILABILIR LAYERLER
     ;; spell-checking
     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     yaml
     (helm :variables
           spacemacs-helm-rg-max-column-number 128)

     emacs-lisp
     (latex :variables
            latex-enable-folding t
            )
     (python :variables
             python-backend 'lsp        ; python-lsp-server 'mspyls
             python-sort-imports-on-save t
             python-formatter 'black
             python-format-on-save t
             python-fill-column 88
             ; lsp-pyls-plugins-jedi-completion-include-params t
             ;; python-formatter 'black
             )

     (ipython-notebook ; :variables
      ;; ein:use-auto-complete t
      ;; ein:complete-on-dot t
      ;; ein:completion-backend 'ein:use-company-backend
      )
     dap
     django
     (auto-completion :variables
                      ;; auto-completion-enable-snippets-in-popup t
                      auto-completion-return-key-behavior nil
                      auto-completion-tab-key-behavior 'complete
                      auto-completion-enable-help-tooltip t  ;; M-h ile oto açma
                      auto-completion-idle-delay 0
                      auto-completion-enable-sort-by-usage t
                      auto-completion-enable-snippets-in-popup t
                      auto-completion-use-company-box t
                      )
     (multiple-cursors :variables
                       multiple-cursors-backend 'mc
                       )
     (org :variables
          org-enable-sticky-header t
          org-sidebar-tree-side 'left
          org-catch-invisible-edits 'show-and-error
          org-confirm-babel-evaluate nil
          org-cycle-separator-lines 0
          ;; sticky header setting
          org-sticky-header-full-path 'full
          )
     sql
     ;; (c-c++ :variables
     ;;        c-c++-backend 'lsp-ccls
     ;;        ;;c-c++-backend 'lsp-cquery
     ;;        ;;c-c++-backend 'rtags
     ;;        ;;c-c++-enable-clang-support t
     ;;        ;; c-c++-enable-clang-support t ignored when useing lsp backend
     ;;        c++-enable-organize-includes-on-save t
     ;;        c-c++-enable-clang-format-on-save t
     ;;        c-c++-lsp-sem-highlight-rainbow t ;; kerhane renkleri galiba
     ;;        c-c++-adopt-subprojects t ;; ccls wikide tavsiye ediyorlar imiş
     ;;        )
     ;; cmake
     (better-defaults :variables
                      ;; better-defaults-move-to-end-of-code-first t
                      )
     git ; TODO: MAGIT OGREN
     (version-control :variables
                      version-control-diff-side 'left)
     syntax-checking
     (html :variables
           css-indent-offset 2
           web-mode-markup-indent-offset 2
           web-mode-css-indent-offset 2
           web-mode-code-indent-offset 2
           web-mode-attr-indent-offset 2
           web-mode-engines-alist '(("django"    . "\\.html\\'"))
           )

     (colors :variables
             colors-colorize-identifiers 'variables)
     ibuffer
     ;; semantic ; interesting
     react
     (restclient :variables
                 restclient-use-org nil)

     (shell :variables
            shell-default-height 35
            shell-default-position 'bottom)
     (javascript :variables
                 node-add-modules-path t
                 javascript-backend 'lsp
                 javascript-fmt-on-save t
                 javascript-lsp-linter nil
                 javascript-fmt-tool 'prettier
                 js2-basic-offset 2
                 js-indent-level 2
                 )
     (lsp :variables
                                        ; lsp-ui-sideline-enable nil
          )

     )




   ;; List of additional packages that will be installed without being
   ;; wrapped in a layer. If you need some configuration for these
   ;; packages, then consider creating a layer. You can also put the
   ;; configuration in `dotspacemacs/user-config'.
   dotspacemacs-additional-packages '(
                                      helm-rg
                                      google-translate
                                      deadgrep
                                      beacon
                                      expand-region
                                      highlight-indent-guides
                                      wakatime-mode
                                      rich-minority
                                      org-sidebar
                                      magit-todos
                                      )
   ;; A list of packages that cannot be updated.
   dotspacemacs-frozen-packages '()
   ;; A list of packages that will not be installed and loaded.
   dotspacemacs-excluded-packages '(importmagic
                                    ;; TODO: import magicte sıkıntı var. kurup kullanmayı öğren
                                    hy-mode
                                    cython-mode
                                    eldoc
                                    ggtags ; ?
                                    helm-gtags
                                    ;; These are diff from base and standart spacemacs versisons
                                    column-enforce-mode
                                    ;; expand-region (ctrl -t)
                                    ;; highlight (?)
                                    paradox
                                    ;; hl-todo  (koddaki todolar)
                                    ;; clean-aindent-mode (boşlukları silen)
                                    neotree
                                    vi-tilde-fringe
                                    evil-unimpaired
                                    anzu ; ?
                                    smartparens ;; (akıllı parantezler)
                                    flx-ido
                                    Aggressive-indent
                                    open-junk-file
                                    evil-surround
                                    linum-relative
                                    fancy-battery
                                    persp-mode
                                    adaptive-wrap
                                    ace-link
                                    evil-args
                                    lorem-ipsum
                                    evil-exchange
                                    spaceline
                                    dumb-jump ;; ?? incele
                                    ;; auto-highlight-symbol
                                    winum
                                    ;; popwin ;; sanırım helmin isine yarıyor NOTE: bir gün spacemasten kaçacaksam lazım olabilir
                                    ;; iedit ;; TODO: iedit nedir ne değildir bi bakıp öğrenmek iyi olabilir
                                    evil-numbers
                                    evil-iedit-state
                                    link-hintn
                                    evil-nerd-commenter
                                    evil-lisp-state
                                    eval-sexp-fu
                                    eyebrowse
                                    evil-tutor
                                    indent-guide ;; güzel
                                    highlight-indentation
                                    evil-indent-plus
                                    volatile-highlights
                                    restart-emacs
                                    highlight-numbers
                                    evil-ediff
                                    evil-visual-mark-mode
                                    evil-mc
                                    define-word
                                    powerline
                                    evil-search-highlight-persist
                                    evil-anzu
                                    ;; google-translate
                                    golden-ratio
                                    lv
                                    fill-column-indicator
                                    )
   ;; Defines the behaviour of Spacemacs when installing packages.
   ;; Possible values are `used-only', `used-but-keep-unused' and `all'.
   ;; `used-only' installs only explicitly used packages and uninstall any
   ;; unused packages as well as their unused dependencies.
   ;; `used-but-keep-unused' installs only the used packages but won't uninstall
   ;; them if they become unused. `all' installs *all* packages supported by
   ;; Spacemacs and never uninstall them. (default is `used-only')
   dotspacemacs-install-packages 'used-only))

(defun dotspacemacs/init ()
  "Initialization function.
This function is called at the very startup of Spacemacs initialization
before layers configuration.
You should not put any user code in there besides modifying the variable
values."
  ;; This setq-default sexp is an exhaustive list of all the supported
  ;; spacemacs settings.
  (setq-default
   ;; If non-nil then enable support for the portable dumper. You'll need
   ;; to compile Emacs 27 from source following the instructions in file
   ;; EXPERIMENTAL.org at to root of the git repository.
   ;; (default nil)
   dotspacemacs-enable-emacs-pdumper t

   ;; File path pointing to emacs 27.1 executable compiled with support
   ;; for the portable dumper (this is currently the branch pdumper).
   ;; (default "emacs-27.0.50")
   dotspacemacs-emacs-pdumper-executable-file "emacs-27.0.50"

   ;; Name of the Spacemacs dump file. This is the file will be created by the
   ;; portable dumper in the cache directory under dumps sub-directory.
   ;; To load it when starting Emacs add the parameter `--dump-file'
   ;; when invoking Emacs 27.1 executable on the command line, for instance:
   ;;   ./emacs --dump-file=~/.emacs.d/.cache/dumps/spacemacs.pdmp
   ;; (default spacemacs.pdmp)
   dotspacemacs-emacs-dumper-dump-file "spacemacs.pdmp"

   ;; If non nil ELPA repositories are contacted via HTTPS whenever it's
   ;; possible. Set it to nil if you have no way to use HTTPS in your
   ;; environment, otherwise it is strongly recommended to let it set to t.
   ;; This variable has no effect if Emacs is launched with the parameter
   ;; `--insecure' which forces the value of this variable to nil.
   ;; (default t)
   dotspacemacs-elpa-https nil

   ;; Maximum allowed time in seconds to contact an ELPA repository.
   dotspacemacs-elpa-timeout 50

   ;; Set `gc-cons-threshold' and `gc-cons-percentage' when startup finishes.
   ;; This is an advanced option and should not be changed unless you suspect
   ;; performance issues due to garbage collection operations.
   ;; (default '(100000000 0.1))
   dotspacemacs-gc-cons '(100000000 0.1)

   ;; If non-nil then Spacelpa repository is the primary source to install
   ;; a locked version of packages. If nil then Spacemacs will install the
   ;; latest version of packages from MELPA. (default nil)
   dotspacemacs-use-spacelpa nil

   ;; If non-nil then verify the signature for downloaded Spacelpa archives.
   ;; (default nil)
   dotspacemacs-verify-spacelpa-archives nil

   ;; If non-nil then spacemacs will check for updates at startup
   ;; when the current branch is not `develop'. Note that checking for
   ;; new versions works via git commands, thus it calls GitHub services
   ;; whenever you start Emacs. (default nil)
   dotspacemacs-check-for-update nil

   ;; If non-nil, a form that evaluates to a package directory. For example, to
   ;; use different package directories for different Emacs versions, set this
   ;; to `emacs-version'.
   dotspacemacs-elpa-subdirectory nil

   ;; One of `vim', `emacs' or `hybrid'.
   ;; `hybrid' is like `vim' except that `insert state' is replaced by the
   ;; `hybrid state' with `emacs' key bindings. The value can also be a list
   ;; with `:variables' keyword (similar to layers). Check the editing styles
   ;; section of the documentation for details on available variables.
   ;; (default 'vim)
   dotspacemacs-editing-style 'emacs

   ;; If non nil output loading progress in `*Messages*' buffer. (default nil)
   dotspacemacs-verbose-loading t

   ;; Specify the startup banner. Default value is `official', it displays
   ;; the official spacemacs logo. An integer value is the index of text
   ;; banner, `random' chooses a random text banner in `core/banners'
   ;; directory. A string value must be a path to an image format supported
   ;; by your Emacs build.
   ;; If the value is nil then no banner is displayed. (default 'official)
   dotspacemacs-startup-banner 'nil

   ;; List of items to show in startup buffer or an association list of
   ;; the form `(list-type . list-size)`. If nil then it is disabled.
   ;; Possible values for list-type are:
   ;; `recents' `bookmarks' `projects' `agenda' `todos'."
   ;; List sizes may be nil, in which case
   ;; `spacemacs-buffer-startup-lists-length' takes effect.
   dotspacemacs-startup-lists '((projects . 6)
                                (recents . 15))

   ;; True if the home buffer should respond to resize events.
   dotspacemacs-startup-buffer-responsive t

   ;; Default major mode of the scratch buffer (default `text-mode')
   dotspacemacs-scratch-mode 'emacs-lisp-mode

   ;; Initial message in the scratch buffer, such as "Welcome to Spacemacs!"
   ;; (default nil)
   dotspacemacs-initial-scratch-message nil

   ;; List of themes, the first of the list is loaded when spacemacs starts.
   ;; Press <SPC> T n to cycle to the next theme in the list (works great
   ;; with 2 themes variants, one dark and one light)
   dotspacemacs-themes '(spacemacs-dark)

   ;; Set the theme for the Spaceline. Supported themes are `spacemacs',
   ;; `all-the-icons', `custom', `doom', `vim-powerline' and `vanilla'. The
   ;; first three are spaceline themes. `doom' is the doom-emacs mode-line.
   ;; `vanilla' is default Emacs mode-line. `custom' is a user defined themes,
   ;; refer to the DOCUMENTATION.org for more info on how to create your own
   ;; spaceline theme. Value can be a symbol or list with additional properties.
   ;; (default '(spacemacs :separator wave :separator-scale 1.5))
   ;; dotspacemacs-mode-line-theme '(spacemacs :separator wave :separator-scale 1.5)
   dotspacemacs-mode-line-theme '(doom)

   ;; If non nil the cursor color matches the state color in GUI Emacs.
   dotspacemacs-colorize-cursor-according-to-state t
   ;; Default font, or prioritized list of fonts. `powerline-scale' allows to
   ;; quickly tweak the mode-line size to make separators look not too crappy.
   ;; dotspacemacs-default-font '("Source Code Pro"
   ;;                             :size 11
   ;;                             :weight normal
   ;;                             :width normal
   ;;                             :powerline-scale 1.1)
   dotspacemacs-default-font '("Source Code Pro"
                               :size 11
                               :weight normal
                               :width normal
                               :powerline-scale 1.1)

   ;; The leader key
   dotspacemacs-leader-key "SPC"
   ;; The key used for Emacs commands (M-x) (after pressing on the leader key).
   ;; (default "SPC")

   dotspacemacs-emacs-command-key "SPC"
   ;; The key used for Vim Ex commands (default ":")
   dotspacemacs-ex-command-key ":"

   ;; The leader key accessible in `emacs state' and `insert state'
   ;; (default "M-m")
   dotspacemacs-emacs-leader-key "M-m"

   ;; Major mode leader key is a shortcut key which is the equivalent of
   ;; pressing `<leader> m`. Set it to `nil` to disable it. (default ",")
   dotspacemacs-major-mode-leader-key ","

   ;; Major mode leader key accessible in `emacs state' and `insert state'.
   ;; (default "C-M-m")
   dotspacemacs-major-mode-emacs-leader-key "C-M-m"

   ;; These variables control whether separate commands are bound in the GUI to
   ;; the key pairs C-i, TAB and C-m, RET.
   ;; Setting it to a non-nil value, allows for separate commands under <C-i>
   ;; and TAB or <C-m> and RET.
   ;; In the terminal, these pairs are generally indistinguishable, so this only
   ;; works in the GUI. (default nil)
   dotspacemacs-distinguish-gui-tab nil

   ;; Name of the default layout (default "Default")
   dotspacemacs-default-layout-name "Default"
   ;; If non nil the default layout name is displayed in the mode-line.
   ;; (default nil)
   dotspacemacs-display-default-layout nil

   ;; If non nil then the last auto saved layouts are resume automatically upon
   ;; start. (default nil)
   dotspacemacs-auto-resume-layouts nil

   ;; If non-nil then the last auto saved layouts are resumed automatically upon
   ;; start. (default nil)
   dotspacemacs-auto-resume-layouts nil

   ;; Size (in MB) above which spacemacs will prompt to open the large file
   ;; literally to avoid performance issues. Opening a file literally means that
   ;; no major mode or minor modes are active. (default is 1)
   dotspacemacs-large-file-size 1

   ;; Location where to auto-save files. Possible values are `original' to
   ;; auto-save the file in-place, `cache' to auto-save the file to another
   ;; file stored in the cache directory and `nil' to disable auto-saving.
   ;; (default 'cache)
   dotspacemacs-auto-save-file-location 'cache

   ;; Maximum number of rollback slots to keep in the cache. (default 5)
   dotspacemacs-max-rollback-slots 5

   ;; If non nil the paste micro-state is enabled. When enabled pressing `p`
   ;; several times cycle between the kill ring content. (default nil)
   dotspacemacs-enable-paste-transient-state nil

   ;; Which-key delay in seconds. The which-key buffer is the popup listing
   ;; the commands bound to the current keystroke sequence. (default 0.4)
   dotspacemacs-which-key-delay 0.4

   ;; Which-key frame position. Possible values are `right', `bottom' and
   ;; `right-then-bottom'. right-then-bottom tries to display the frame to the
   ;; right; if there is insufficient space it displays it at the bottom.
   ;; (default 'bottom)
   dotspacemacs-which-key-position 'bottom

   ;; Control where `switch-to-buffer' displays the buffer. If nil,
   ;; `switch-to-buffer' displays the buffer in the current window even if
   ;; another same-purpose window is available. If non-nil, `switch-to-buffer'
   ;; displays the buffer in a same-purpose window even if the buffer can be
   ;; displayed in the current window. (default nil)
   dotspacemacs-switch-to-buffer-prefers-purpose nil

   ;; If non nil a progress bar is displayed when spacemacs is loading. This
   ;; may increase the boot time on some systems and emacs builds, set it to
   ;; nil to boost the loading time. (default t)
   dotspacemacs-loading-progress-bar nil

   ;; If non nil the frame is fullscreen when Emacs starts up. (default nil)
   ;; (Emacs 24.4+ only)
   dotspacemacs-fullscreen-at-startup nil

   ;; If non nil `spacemacs/toggle-fullscreen' will not use native fullscreen.
   ;; Use to disable fullscreen animations in OSX. (default nil)
   dotspacemacs-fullscreen-use-non-native nil

   ;; If non nil the frame is maximized when Emacs starts up.
   ;; Takes effect only if `dotspacemacs-fullscreen-at-startup' is nil.
   ;; (default nil) (Emacs 24.4+ only)
   dotspacemacs-maximized-at-startup t

   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's active or selected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-active-transparency 90

   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's inactive or deselected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-inactive-transparency 90

   ;; If non nil show the titles of transient states. (default t)
   dotspacemacs-show-transient-state-title t

   ;; If non nil show the color guide hint for transient state keys. (default t)
   dotspacemacs-show-transient-state-color-guide t

   ;; If non nil unicode symbols are displayed in the mode line. (default t)
   dotspacemacs-mode-line-unicode-symbols t

   ;; If non nil smooth scrolling (native-scrolling) is enabled. Smooth
   ;; scrolling overrides the default behavior of Emacs which recenters point
   ;; when it reaches the top or bottom of the screen. (default t)
   dotspacemacs-smooth-scrolling nil

   ;; Control line numbers activation.
   ;; If set to `t' or `relative' line numbers are turned on in all `prog-mode' and
   ;; `text-mode' derivatives. If set to `relative', line numbers are relative.
   ;; This variable can also be set to a property list for finer control:
   ;; '(:relative nil
   ;;   :disabled-for-modes dired-mode
   ;;                       doc-view-mode
   ;;                       markdown-mode
   ;;                       org-mode
   ;;                       pdf-view-mode
   ;;                       text-mode
   ;;   :size-limit-kb 1000)
   ;; (default nil)
   dotspacemacs-line-numbers nil

   ;; Code folding method. Possible values are `evil' and `origami'.
   ;; (default 'evil)
   dotspacemacs-folding-method 'evil

   ;; If non-nil smartparens-strict-mode will be enabled in programming modes.
   ;; (default nil)
   dotspacemacs-smartparens-strict-mode nil

   ;; If non-nil pressing the closing parenthesis `)' key in insert mode passes
   ;; over any automatically added closing parenthesis, bracket, quote, etc…
   ;; This can be temporary disabled by pressing `C-q' before `)'. (default nil)
   dotspacemacs-smart-closing-parenthesis nil

   ;; Select a scope to highlight delimiters. Possible values are `any',
   ;; `current', `all' or `nil'. Default is `all' (highlight any scope and
   ;; emphasis the current one). (default 'all)
   dotspacemacs-highlight-delimiters 'all

   ;; If non-nil, start an Emacs server if one is not already running.
   ;; (default nil)
   dotspacemacs-enable-server nil

   ;; Set the emacs server socket location.
   ;; If nil, uses whatever the Emacs default is, otherwise a directory path
   ;; like \"~/.emacs.d/server\". It has no effect if
   ;; `dotspacemacs-enable-server' is nil.
   ;; (default nil)
   dotspacemacs-server-socket-dir nil

   ;; If non-nil, advise quit functions to keep server open when quitting.
   ;; (default nil)
   dotspacemacs-persistent-server nil

   ;; List of search tool executable names. Spacemacs uses the first installed
   ;; tool of the list. Supported tools are `rg', `ag', `pt', `ack' and `grep'.
   ;; (default '("rg" "ag" "pt" "ack" "grep"))
   dotspacemacs-search-tools '("rg" "ag" "pt" "ack" "grep")

   ;; Format specification for setting the frame title.
   ;; %a - the `abbreviated-file-name', or `buffer-name'
   ;; %t - `projectile-project-name'
   ;; %I - `invocation-name'
   ;; %S - `system-name'
   ;; %U - contents of $USER
   ;; %b - buffer name
   ;; %f - visited file name
   ;; %F - frame name
   ;; %s - process status
   ;; %p - percent of buffer above top of window, or Top, Bot or All
   ;; %P - percent of buffer above bottom of window, perhaps plus Top, or Bot or All
   ;; %m - mode name
   ;; %n - Narrow if appropriate
   ;; %z - mnemonics of buffer, terminal, and keyboard coding systems
   ;; %Z - like %z, but including the end-of-line format
   ;; (default "%I@%S")
   dotspacemacs-frame-title-format "%I@%S"

   ;; Format specification for setting the icon title format
   ;; (default nil - same as frame-title-format)
   dotspacemacs-icon-title-format nil

   ;; Delete whitespace while saving buffer. Possible values are `all'
   ;; to aggressively delete empty line and long sequences of whitespace,
   ;; `trailing' to delete only the whitespace at end of lines, `changed' to
   ;; delete only whitespace for changed lines or `nil' to disable cleanup.
   ;; (default nil)
   dotspacemacs-whitespace-cleanup nil

   ;; Either nil or a number of seconds. If non-nil zone out after the specified
   ;; number of seconds. (default nil)
   dotspacemacs-zone-out-when-idle nil

   ;; Run `spacemacs/prettify-org-buffer' when
   ;; visiting README.org files of Spacemacs.
   ;; (default nil)
   dotspacemacs-pretty-docs nil))

(defun dotspacemacs/user-env ()
  "Environment variables setup.
This function defines the environment variables for your Emacs session. By
default it calls `spacemacs/load-spacemacs-env' which loads the environment
variables declared in `~/.spacemacs.env' or `~/.spacemacs.d/.spacemacs.env'.
See the header of this file for more information."
  (spacemacs/load-spacemacs-env))


(defun dotspacemacs/user-init ()
  "Initialization function for user code.
It is called immediately after `dotspacemacs/init', before layer configuration
executes.
 This function is mostly useful for variables that need to be set
before packages are loaded. If you are unsure, you should try in setting them in
`dotspacemacs/user-config' first."
  ;; [global]
  (setq doom-modeline-buffer-modification-icon nil)
  (setq-default
   ;; Google translate
   google-translate-default-source-language "en"
   google-translate-default-target-language "tr"
   ;; doom mode line
   doom-modeline-bar-width 1
   doom-modeline-height 1
   doom-modeline-buffer-encoding nil
   doom-modeline-buffer-modification-icon t
   doom-modeline-vcs-max-length 20
   doom-modeline-icon t
   doom-modeline-buffer-file-name-style 'relative-to-project
   ;; UNDO TREE
   undo-limit 800000
   undo-strong-limit 12000000
   undo-outer-limit 120000000
   ;; beacon
   beacon-color "#2FB90E"
   ;; emacs default settings
   create-lockfiles nil
   ;; ORG
   org-drill-overdue-interval-factor 1.1
   ;; neden bunların olduğunu hatırlamıyorum valla
   rm-whitelist       (format "^ \\(%s\\)$"
                              (mapconcat #'identity
                                         '("")
                                         "\\|")))
  )

(defun dotspacemacs/user-load ()
  "Library to load while dumping.
This function is called only while dumping Spacemacs configuration. You can
`require' or `load' the libraries of your choice that will be included in the
dump."
  )


(defun dotspacemacs/user-config ()
  "Configuration function for user code.
This function is called at the very end of Spacemacs initialization after
layers configuration.
This is the place where most of your configurations should be done. Unless it is
explicitly specified that a variable should be set before a package is loaded,explicitly specified that a variable should be set before a package is loaded,explicitly specified that a variable should be set before a package is loaded,
you should place your code here."
  (setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")
  ;; (gnutls-available-p)
  
  ;; DOOM Mode lin settings
  (evil-mode -1) ;; this is for doom mode line
  (set-face-attribute 'mode-line nil :height 80)
  (set-face-attribute 'mode-line-inactive nil :height 80)
  
  (add-hook 'doc-view-mode-hook 'auto-revert-mode)
  


  ;; GLOBAL KEY BINDINGS [global]
  (move-text-default-bindings)

  ;; (setq helm-source-do-ag
  ;;       (helm-build-async-source "The Silver Searcher"
  ;;         :init 'helm-ag--do-ag-set-command
  ;;         :candidates-process 'helm-ag--do-ag-candidate-process
  ;;         :persistent-action  'helm-ag--persistent-action
  ;;         :action helm-ag--actions
  ;;         :nohighlight t
  ;;         :requires-pattern 3
  ;;         :candidate-number-limit 800
  ;;         :keymap helm-do-ag-map
  ;;         :follow (and helm-follow-mode-persistent 1)))
  ;; (global-set-key (kbd "<C-tab>") #'deadgrep)
  ;; (global-set-key (kbd "<C-tab>") #'spacemacs/helm-project-do-rg-region-or-symbol)
  (global-set-key (kbd "<C-tab>") #'helm-rg)
  (setq helm-rg-default-directory 'git-root)
  (setq helm-rg--extra-args '("--max-columns" "200"))


  (global-set-key (kbd "<C-iso-lefttab>") #'deadgrep)
  (global-set-key "\C-xf" 'helm-projectile)
  (global-set-key "\C-t" 'er/expand-region)
  (global-set-key "\M-u" 'winner-undo)
  (global-set-key "\M-y" 'helm-show-kill-ring)
  (global-set-key "\M-o" 'other-window)
  (keyboard-translate ?\C-h ?\C-?) ; C-h silme
  (keyboard-translate ?\C-? ?\C-h) ; C-h ile silme
  (global-set-key (kbd "M-ı") 'spacemacs/indent-region-or-buffer)
  (global-set-key (kbd "M-m w s") 'ace-swap-window)
  ;; (global-set-key (kbd "M-ü") 'yapfify-region)
  
  ;; GLOBAL MODES [global]
  (global-subword-mode 1)
  (load "yasnippet-snippets")
  (global-company-mode)
  (beacon-mode 1)
  (global-diff-hl-mode 1)
  (diff-hl-flydiff-mode 1)
  (delete-selection-mode 1)
  (show-paren-mode 1)
  (rich-minority-mode 1)
  (add-hook 'prog-mode-hook 'wakatime-mode)
  ;; (spacemacs/toggle-indent-guide-globally-on)
  ;; (add-hook 'before-save-hook 'spacemacs/indent-region-or-buffer)
  ;; (spacemacs/toggle-indent-guide-globally-on)
  ;; (add-hook 'prog-mode-hook 'highlight-indent-guides-mode)

  
  (add-hook 'eshell-mode-hook (local-unset-key (kbd "C-l")))

  ;; GLOBAL SETTING [global]
  (fringe-mode '(6 . 0))

  
  ;; (add-hook 'python-mode-hook
  ;;           (spacemacs/toggle-automatic-symbol-highlight-on)
  ;;           )

  (add-hook 'emacs-lisp-mode
            (spacemacs/toggle-automatic-symbol-highlight-on)
            )

  ;; ORG MODE SETTINGS [org]
  (with-eval-after-load 'org
    (setq visual-line-mode t)
    (org-defkey org-mode-map [(meta return)] 'org-meta-return)  ;; The actual fix
    (org-defkey org-mode-map (kbd "C-c C-ı") 'kadir/org/table-sort-by-int)
    (defun kahredici/open-org-side-bar()
      (interactive)
      (progn
        (org-sidebar)
        (org-sidebar-tree)
        )
      )
    (defun kadir/org/table-sort-by-int ()
      (interactive)
      (org-table-iterate 3)
      (org-table-sort-lines t ?N)
      )

    (org-babel-do-load-languages
     'org-babel-load-languages
     '((emacs-lisp . t)
       (python . t)
       ))
    )
  
  (progn
    ;; MULTIPLE CURSORS
    (global-set-key (kbd "C-M-n") 'mc/mark-next-word-like-this)
    (global-set-key (kbd "C-M-p") 'mc/mark-previous-like-this)
    (global-set-key (kbd "C-M-S-n") 'mc/skip-to-next-like-this)
    (global-set-key (kbd "C-M-S-p") 'mc/skip-to-previous-like-this)
    (global-set-key (kbd "C-S-N") 'mc/unmark-previous-like-this)
    (global-set-key (kbd "C-S-P") 'mc/unmark-next-like-this)
    (global-set-key (kbd "C-M-<mouse-1>") 'mc/add-cursor-on-click)
    )

  
  ;; dilogret io
  (setq org-capture-templates
        '(("w" "Todo" entry
           (file+headline "~/Dropbox/org/words.org" "Words")
           "* %?  :drill: \n %i")
          ))

  (defun buffer-whole-string (buffer)
    (with-current-buffer buffer
      (save-restriction
        (widen)
        (buffer-substring-no-properties (point-min) (point-max)))))

  (defun run-dilogret.io (string)
    (progn
      (google-translate-translate "en" "tr" string)
      (pupo/close-window)
      (org-capture-string  (buffer-whole-string (get-buffer-create "*Google Translate*")) "w")
      )
    )

  (defun kadir/dilogret.io (start end)
    (interactive "r")
    (if (use-region-p)
        (run-dilogret.io (buffer-substring start end))
      (run-dilogret.io (word-at-point)))
    )
  (word-at-point)
  
  ;; ORG-SIDEBAR SETTINGS [org-sidebar]
  (with-eval-after-load 'org-sidebar
    (message "sidebar settings..")
    (defun kahredici/org-sidebar-jump-and-back()
      (interactive)
      (progn
        (org-sidebar-tree-jump)
        (other-window -1)
        )
      )
    (define-key org-sidebar-tree-map (kbd "C-o") 'kahredici/org-sidebar-jump-and-back)
    (define-key org-sidebar-tree-map (kbd "C-m") 'org-sidebar-tree-jump)
    )
  
  ;; COMPANY MODE SETTINGS [company]
  (with-eval-after-load 'company
    (define-key company-mode-map (kbd "C-.") 'helm-company)
    (define-key company-active-map (kbd "C-f") 'forward-char)
    )
  
  ;; EMMET (HTML - JS) SETTING [js][emmet]
  (with-eval-after-load 'emmet-mode
    ;; TODO: yeni spacmas versiyonunda tabın js ile çakışma problemi kalmamış olabilir. incele.
    (evil-define-key 'emacs emmet-mode-keymap (kbd "TAB") 'evil-indent-line)
    (evil-define-key 'emacs emmet-mode-keymap (kbd "<tab>") 'evil-indent-line)
    )
  (with-eval-after-load "term"
    (define-key term-raw-map "\M-o" 'other-window)
    )
  
                                        ; Magit todos
  (magit-todos-mode t)
  (defun furkan/helm-magit-todos-without-magit-status ()
    (interactive)
    (magit-status)
    (bury-buffer)
    (winner-undo)
    (helm-magit-todos)
    )
  
  ;; JS SETTING [js]
  (with-eval-after-load 'js2-mode
    (define-key js2-mode-map (kbd "M-.") 'lsp-ui-peek-find-definitions)
    (define-key js2-mode-map (kbd "M-e") 'lsp-ui-flycheck-list)
    (defun kahredici/react/eslint-fix-and-revert ()
      (interactive)
      (start-process-shell-command "p"
                                   nil
                                   (concat "eslint --fix " (buffer-file-name))
                                   )
      (revert-buffer t t))
    )
  
  ;; JS SETTING [js]
  (with-eval-after-load 'rjsx-mode
    ;; (defun lsp--eldoc-message (&optional msg)
    ;;   "Show MSG in eldoc."
    ;;   (run-with-idle-timer 0 nil (lambda () (eldoc-message (substring msg 0 180)))))
    ;; (defun lsp--eldoc-message (&optional msg)
    ;;   "Show MSG in eldoc."
    ;;   (run-with-idle-timer 0 nil (lambda () (eldoc-message msg))))
    )
  )

;; Do not write anything past this comment. This is where Emacs will
;; auto-generate custom variable definitions.

(defun dotspacemacs/emacs-custom-settings ()
  "Emacs custom settings.
This is an auto-generated function, do not modify its content directly, use
Emacs customize menu instead.
This function is called at the very end of Spacemacs initialization."
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-modules '(org-drill))
 '(package-selected-packages
   '(ox-twbs helm-rg helm-rtags google-c-style flycheck-rtags disaster cquery cpp-auto-include company-rtags rtags company-c-headers clang-format ccls selectric-mode org-sticky-header utop tuareg caml seeing-is-believing rvm ruby-tools ruby-test-mode ruby-refactor ruby-hash-syntax rubocopfmt rubocop rspec-mode robe rbenv rake ocp-indent ob-elixir mvn minitest meghanada maven-test-mode lsp-java groovy-mode groovy-imports pcache gradle-mode flycheck-ocaml merlin flycheck-mix flycheck-credo emojify emoji-cheat-sheet-plus dune company-emoji chruby bundler inf-ruby auto-complete-rst alchemist elixir-mode dim rich-minority wakatime-mode company-flx org-plus-contrib exwm-firefox-core exwm-x exwm xelb w3 elfeed-web elfeed-org elfeed-goodies ace-jump-mode noflet elfeed org-trello request-deferred deferred highlight-indent-guides restclient-test yaml-mode ob-restclient ob-http company-restclient know-your-http-well web-beautify livid-mode skewer-mode simple-httpd json-mode json-snatcher json-reformat js2-refactor multiple-cursors js2-mode js-doc company-tern tern coffee-mode ws-butler winum volatile-highlights vi-tilde-fringe uuidgen toc-org spaceline powerline restart-emacs rainbow-delimiters popwin persp-mode paradox lv org-bullets open-junk-file neotree move-text lorem-ipsum linum-relative link-hint indent-guide hungry-delete hl-todo highlight-parentheses highlight-numbers parent-mode highlight-indentation google-translate golden-ratio flx-ido fill-column-indicator fancy-battery eyebrowse evil-visual-mark-mode evil-unimpaired evil-tutor evil-surround evil-search-highlight-persist highlight evil-numbers evil-nerd-commenter evil-mc evil-matchit evil-lisp-state smartparens evil-indent-plus evil-iedit-state iedit evil-exchange evil-ediff evil-args evil-anzu anzu eval-sexp-fu dumb-jump define-word column-enforce-mode clean-aindent-mode auto-highlight-symbol aggressive-indent adaptive-wrap ace-link expand-region beacon deadgrep spinner xterm-color shell-pop multi-term eshell-z eshell-prompt-extras esh-help stickyfunc-enhance srefactor ibuffer-projectile rainbow-mode rainbow-identifiers color-identifiers-mode git-gutter-fringe+ git-gutter-fringe fringe-helper git-gutter+ git-gutter diff-hl web-mode tagedit slim-mode scss-mode sass-mode pug-mode helm-css-scss haml-mode emmet-mode company-web web-completion-data smeargle orgit magit-gitflow magit-popup helm-gitignore request gitignore-mode gitconfig-mode gitattributes-mode git-timemachine git-messenger git-link evil-magit magit transient git-commit with-editor unfill mwim org-projectile org-category-capture org-present org-pomodoro alert log4e gntp org-mime org-download htmlize gnuplot helm-company helm-c-yasnippet fuzzy company-statistics company-quickhelp pos-tip company-anaconda company auto-yasnippet yasnippet ac-ispell auto-complete yapfify pyvenv pytest pyenv-mode py-isort pony-mode pip-requirements live-py-mode hy-mode dash-functional helm-pydoc cython-mode anaconda-mode pythonic f dash s which-key use-package pcre2el macrostep hydra helm-themes helm-swoop helm-projectile helm-mode-manager helm-make helm-flx helm-descbinds helm-ag exec-path-from-shell evil-visualstar evil-escape elisp-slime-nav diminish bind-map auto-compile ace-window ace-jump-helm-line))
 '(safe-local-variable-values
   '((magit-todos-exclude-globs . static/)
     (typescript-backend . tide)
     (typescript-backend . lsp)
     (javascript-backend . tern)
     (javascript-backend . lsp))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ahs-definition-face ((t (:underline t :weight bold :background nil :foreground nil))))
 '(ahs-face ((t (:underline t :weight bold :background nil :foreground nil))))
 '(ahs-plugin-whole-buffer-face ((t (:underline t :weight bold :background nil :foreground nil))))
 '(helm-rg-active-arg-face ((t (:underline nil :background nil :foreground nil))))
 '(helm-rg-base-rg-cmd-face ((t (:underline nil :background nil :foreground nil))))
 '(helm-rg-directory-cmd-face ((t (:underline nil :background nil :foreground nil))))
 '(helm-rg-directory-header-face ((t (:underline nil :background nil :foreground nil))))
 '(helm-rg-error-message ((t (:underline nil :background nil :foreground nil))))
 '(helm-rg-extra-arg-face ((t (:underline nil :background nil :foreground nil))))
 '(helm-rg-file-match-face ((t (:underline t :weight bold :background nil :foreground "#669A9A"))))
 '(helm-rg-inactive-arg-face ((t (:underline nil :background nil :foreground nil))))
 '(helm-rg-match-text-face ((t (:underline nil :background nil :foreground nil))))
 '(helm-rg-title-face ((t (:underline nil :background nil :foreground nil))))
 '(lsp-face-highlight-read ((t (:underline t :weight bold))))
 '(lsp-face-highlight-write ((t (:underline t :weight bold)))))
)
